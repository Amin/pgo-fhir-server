FROM node:10.15.1

RUN useradd --user-group --create-home --shell /bin/false app
ENV HOME=/home/app

ENV NODE_ENV production

WORKDIR $HOME/webapp
RUN chown -R app:app $HOME/*
USER app
COPY package.json .
COPY yarn.lock .
RUN yarn install --prod

COPY . $HOME/webapp

EXPOSE 3000
CMD yarn start
